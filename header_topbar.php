<div class="gbtr_tools_wrapper">
	<div class="container_12">
		<div class="grid_5 call-us">
			<p style="padding: 8px; color: #f4f4f4; "><a id="your-account" href="http://www.grantcardone.com/my-account/">My Account</a> &nbsp; | &nbsp; Call Us Today: <a href="tel:+13107770255">310-777-0255</a></p>
		</div>
		<div class="grid_7">
                <div class="social-header">
                    <a class="tw" href="http://twitter.com/grantcardone" target="_blank">Twitter</a><a class="fb" href="http://facebook.com/grantcardonefan" target="_blank">Facebook</a><a class="gp" href="https://plus.google.com/106268128704132883440" target="_blank">Google+</a><a class="li" href="http://www.linkedin.com/in/grantcardone" target="_blank">LinkedIn</a><a class="yt" href="http://www.youtube.com/user/GrantCardone" target="_blank">YouTube</a>
                </div>


                <div class="gbtr_tools_search">
                    <form method="get" action="<?php echo home_url(); ?>">
                        <input class="gbtr_tools_search_inputtext" type="text" value="<?php echo esc_html($s, 1); ?>" name="s" id="s" />
                        <input class="gbtr_tools_search_inputbutton" type="submit" value="Search" />
                        <?php 
                        /**
                        * Check if WooCommerce is active
                        **/
                        if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
                        ?>
                        <input type="hidden" name="post_type" value="product">
                        <?php } ?>
                    </form>
                </div>
                <div class="gbtr_tools_account">
                    <ul>
                        <?php if ( has_nav_menu( 'tools' ) ) : ?>
                        <?php  
                        wp_nav_menu(array(
                            'theme_location' => 'tools',
                            'container' =>false,
                            'menu_class' => '',
                            'echo' => true,
                            'items_wrap'      => '%3$s',
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'depth' => 0,
                            'fallback_cb' => false,
                        ));
                        ?>
                        <?php else: ?>
                            Define your top bar navigation.
                        <?php endif; ?>
                    </ul>
                </div>               
            </div>
        </div><!--.container-12-->
        
        <?php if ( $top_bar_menu_items > 2 ) : ?>
        	
        	<div class="gbtr_tools_account mobile <?php echo $top_bar_menu_items > 2 ? 'menu-hidden' : '';?>">
        		<ul class="topbar-menu">
        			<?php if ( has_nav_menu( 'tools' ) ) : ?>
        				<?php  
        				wp_nav_menu(array(
        					'theme_location' => 'tools',
        					'container' =>false,
        					'menu_class' => '',
        					'echo' => true,
        					'items_wrap'      => '%3$s',
        					'before' => '',
        					'after' => '',
        					'link_before' => '',
        					'link_after' => '',
        					'depth' => 0,
        					'fallback_cb' => false,
        					));
        					?>
        					
        				<?php else: ?>
        					<li>Define your top bar navigation.</li>
        				<?php endif; ?>
        			</ul>
        		</div><!--.gbtr_tools_account-->
        		
        	<?php endif; ?>
        	
        </div>