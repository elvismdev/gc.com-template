<?php
/*
Template Name: 100% Width - University
*/
?>

<?php get_header(); ?>

<style type="text/css">
.gbtr_tools_wrapper, .gbtr_header_wrapper {
	display: none;
}

.cardone_header_wrapper {
	padding-top: 39px;
	padding-bottom: 40px;
	background-color: #f4f4f4;
	margin-bottom: 30px;
	border-bottom: 5px solid #F89938;
}

.rev_slider_wrapper {
	z-index: 0;
}

.intro_video {
	z-index: 1;
	position: relative;
	margin-top: -100px;
	border: 10px solid #ffffff;
	-webkit-box-shadow:  0px 0px 10px 0px rgba(0, 0, 0, .25);
	box-shadow:  0px 0px 10px 0px rgba(0, 0, 0, .25);
}

.number h1 {
	font-family: 'Conv_HelveticaNeueLTPro-Cn';
	font-size: 40px;
	color: #115E9E;
	text-align: right;
	padding: 24px 0 0 0;
}

.inputB {
	background: #f4f4f4;
	border: none;
	padding: 7px;
	font-family: "Helvetica Neue", Helvetica, Arial;
	font-size: 12px;
	width: 94%;
}

a.image-video {
	width: 100%;
	position: relative;
	display: block;
}

a.image-video span.icon-play {
	position: absolute;
	left: 0;
	top: 0;
	filter: alpha(opacity=50);
	-moz-opacity: 0.5;
	-khtml-opacity: 0.5;
	opacity: 0.5;
}

a.image-video:hover span.icon-play {
	filter: alpha(opacity=90);
	-moz-opacity: 0.9;
	-khtml-opacity: 0.9;
	opacity: 0.9;
}

a.image-video .title-video {
	width: 100%;
	font-family: 'Conv_HelveticaNeueLTPro-BdCn';
	color: #ffffff;
	font-size: 24px;
	position: relative;
	top: -30px;
	margin: 0 auto;
	text-align: center;
	text-shadow: 0px 0px 20px #000000;
	filter: dropshadow(color=#000000, offx=0, offy=0);
	text-transform: uppercase;
	filter: alpha(opacity=95);
	-moz-opacity: 0.95;
	-khtml-opacity: 0.95;
	opacity: 0.95;
}

#free-trial {
	font-family: 'Conv_HelveticaNeueLTPro-BdCn';
	font-size: 46px;
	padding: 0;
	color: #1e64a3 !important;
}

#insightly_firstName, #insightly_lastName, #insightly_organization, #insightly_role, #insightly_background, .insightly_email, .insightly_phone  {
	background: #f4f4f4;
	border: none;
	padding: 7px;
	font-family: "Helvetica Neue", Helvetica, Arial;
	font-size: 12px;
	margin: 0 5px 5px 0;
	width: 29.5%;
}

.modal h2 {
	color: #000000;
	text-align: center;
	padding: 7px 0 15px;
	font-family: "Helvetica Neue", Helvetica, Arial;
}

.modal {
	padding: 20px;
}

.modal p {
	color: #000000;
	margin: 0 0 4px;
	font-size: 10px;
}

.modal iframe {
	z-index: 0;
}

.modal .form {
	z-index: 1;
	position: relative;
}

#sb-wrapper-inner {
	border: 0 !important;
}



@media only screen and (max-width: 479px) {
	#insightly_firstName, #insightly_lastName, #insightly_organization, #insightly_role, #insightly_background, .insightly_email, .insightly_phone {
		width: 97%;
	}
	.modal {
		margin-top: -40px !important;
	}
	.modal .form {
		margin-top: -38px !important;
	}

}

</style>

<div class="cardone_header_wrapper">
	<div class="container_12">
		<div class="grid_6 number">
			<img src="http://www.grantcardone.com/wp-content/uploads/cardone-logo-university.png" width="100%"/>
		</div>
		<div class="grid_6 number">
			<h1>800-368-5771</h1>
		</div>
	</div>
</div>

	<?php while ( have_posts() ) : the_post(); ?>
        
        <div class="page_full_width">
            <div class="entry-content">
                <div class="">
                	<?php echo do_shortcode( '[rev_slider university]' ) ?>
	                	<div class="shortcode_container">

                    <!-- jQuery.js -->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>

					<link rel="stylesheet" type="text/css" href="http://grantcardone.com/wp-content/themes/theretailer-child/js/shadowbox/shadowbox.css">
					<script type="text/javascript" src="http://grantcardone.com/wp-content/themes/theretailer-child/js/shadowbox/shadowbox.js"></script>
					
						<script type="text/javascript">
						Shadowbox.init({
						    handleOversize: "drag",
						    modal: true,
						    enableKeys: false
						});
						</script>

						<!-- <script type="text/javascript">
						$(function() {
							window.setTimeout(function() {
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            }); } , 21000); }); </script> -->

						<script type="text/javascript">
						$(function() {
							$('#get-started').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#get-started"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#sales').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78107670?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#sales"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#customer-service').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109584?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#customer-service"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#negotiations').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109664?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#negotiations"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#mastering-phones').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109609?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#mastering-phones"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#prospecting').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109663?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#prospecting"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#communications').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109713?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#communications"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#rules-of-success').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109721?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#rules-of-success"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#motivation').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/78109745?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><input type="hidden" id="websites[0]_Value" name="websites[0].Value" value="http://cardoneuniversity.com/#motivation"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<br class="clear" />

						<script>
						function validateForm() {
							var x=document.forms["insightly_web_to_contact"]["FirstName"].value;
							if (x==null || x=="") {
								alert("First Name must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["LastName"].value;
							if (x==null || x=="") {
								alert("Last Mame must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["Organization"].value;
							if (x==null || x=="") {
								alert("Organization must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["Role"].value;
							if (x==null || x=="") {
								alert("Role must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["emails[0].Value"].value;
							if (x==null || x=="") {
								alert("Email must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["phones[0].Value"].value;
							if (x==null || x=="") {
								alert("Phone must be filled out.");
								return false;
							}
						}
						</script>					

                    <br class="clear" />


                    <div class="content_grid_3">
                    	<h2 style="padding: 10px 0;">Start Converting Now!</h2>
                    	<p style="font-size: 10px; padding: 0;">Request a</p>
                    	<h2 id="free-trial">FREE TRIAL</h2>
                    	<a  id="get-started" class="button" style="text-align: right;" href="#">Get Started Here</a>
                    </div>

                    <div class="content_grid_6">
                    	<div class="intro_video" style="max-width:460px;">
                    		<iframe src="//player.vimeo.com/video/78642802?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    	</div>
                    </div>

                    <div class="content_grid_3">
                    	<h2 style="padding: 10px 0;">Member Login</h2>
                    	<form action="https://login.lightspeedvt.com/actions.cfm?lg=357" method="POST">
							<input type="hidden" name="action" value="login">
							<input class="inputB" name="username" type="text" placeholder="Username">
							<input class="inputB" name="password" type="password" placeholder="Password">
							<input type="submit" class="button" style="width: 80px; float: right; margin: 3px 0 0 0;" value="Login">
							<a style="font-size: 10px;" href="http://login.lightspeedvt.com/forgot_password.cfm?lg=21" target="_blank">Forgot your password?</a>
						</form>
                    </div>



                    <br class="clear" />

                    <div class="content_hr" style="margin-top:30px;margin-bottom:24px"></div>

                    <div id="social">
                        <div id="social-center">
                            <div class="twitter" data-url="http://www.cardoneondemand.com" data-text="<?php the_title() ?>" data-title="Tweet" title="Twitter"></div>
                            <div class="facebook" data-url="http://www.cardoneondemand.com" data-text="<?php the_title() ?>" data-title="Like" title="Facebook"></div>
                            <div class="googleplus" data-url="http://www.cardoneondemand.com" data-text="<?php the_title() ?>" data-title="+1" title="Google Plus"></div>
                            <div class="linkedin" data-url="http://www.cardoneondemand.com" data-text="<?php the_title() ?>" data-title="Share" title="LinkedIn"></div>
                        </div>
                    </div>

                    <div class="content_hr" style="margin-top:28px;margin-bottom:10px"></div>

	                	<br class="clear" />



	                	<div class="content_grid_3" id="sales">
							<a class="image-video" href="#" rel="" title="Sales">
								<img src="http://www.grantcardone.com/wp-content/uploads/cardone-university-page-1.jpg" width="100%" alt="Sales" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
								<div class="title-video">Sales</div>
							</a>
						</div>

	                	<div class="content_grid_3" id="customer-service">
							<a class="image-video" href="#" rel="" title="Customer Service">
								<img src="http://www.grantcardone.com/wp-content/uploads/cardone-university-page-2.jpg" width="100%" alt="Customer Service" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
								<div class="title-video">Customer Service</div>
							</a>
						</div>

	                	<div class="content_grid_3" id="negotiations">
							<a class="image-video" href="#" rel="" title="Negotiations">
								<img src="http://www.grantcardone.com/wp-content/uploads/cardone-university-page-3.jpg" width="100%" alt="Negotiations" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
								<div class="title-video">Negotiations</div>
							</a>
						</div>

	                	<div class="content_grid_3" id="mastering-phones">
							<a class="image-video" href="#" rel="" title="Mastering Phones">
								<img src="http://www.grantcardone.com/wp-content/uploads/cardone-university-page-4.jpg" width="100%" alt="Mastering Phones" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
								<div class="title-video">Mastering Phones</div>
							</a>
						</div>

						<br class="clear" />
	                	
	                	<div class="content_grid_3" id="prospecting">
							<a class="image-video" href="#" rel="" title="Prospecting">
								<img src="http://www.grantcardone.com/wp-content/uploads/cardone-university-page-5.jpg" width="100%" alt="Prospecting" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
								<div class="title-video">Prospecting</div>
							</a>
						</div>

	                	<div class="content_grid_3" id="communications">
							<a class="image-video" href="#" rel="" title="Communications">
								<img src="http://www.grantcardone.com/wp-content/uploads/cardone-university-page-6.jpg" width="100%" alt="Communications" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
								<div class="title-video">Communications</div>
							</a>
						</div>

	                	<div class="content_grid_3" id="rules-of-success">
							<a class="image-video" href="#" rel="" title="Rules of Success">
								<img src="http://www.grantcardone.com/wp-content/uploads/cardone-university-page-7.jpg" width="100%" alt="Rules of Success" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
								<div class="title-video">Rules of Success</div>
							</a>
						</div>

	                	<div class="content_grid_3" id="motivation">
							<a class="image-video" href="#" rel="" title="Motivation">
								<img src="http://www.grantcardone.com/wp-content/uploads/cardone-university-page-8.jpg" width="100%" alt="Motivation" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
								<div class="title-video">Motivation</div>
							</a>
						</div>

	                </div>

	                	<br class="clear" />
						<br class="clear" />

	                	<?php echo do_shortcode( '[ditty_news_ticker id="11716"]' ) ?>
	                    <br class="clear" />                
	                	<br class="clear" />

                </div>
            </div><!-- .entry-content -->
        </div>

    <?php endwhile; // end of the loop. ?>

<?php get_template_part("light_footer"); ?>
<?php get_template_part("dark_footer"); ?>

<?php get_footer(); ?>