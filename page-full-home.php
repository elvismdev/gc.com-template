<?php
/*
Template Name: 100% Width - Home
*/
?>

<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
        
        <div class="page_full_width">
            <div class="entry-content">
                <div class="">

                	<?php echo do_shortcode( '[rev_slider home]' ) ?>

                	<?php echo do_shortcode( '[ditty_news_ticker id="8873"]' ) ?>

                	<div class="empty_separator" style="margin-top:10px;margin-bottom:30px"></div>

                	<div class="shortcode_container">



                    <!-- jQuery.js -->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>

                    <br class="clear" />

                    <?php the_content(); ?>


                	</div>
                	
                	<?php echo do_shortcode( '[ditty_news_ticker id="11716"]' ) ?>
                    <br class="clear" />                
                	<br class="clear" />              	

                </div>
            </div><!-- .entry-content -->
        </div>

    <?php endwhile; // end of the loop. ?>

<?php get_template_part("light_footer"); ?>
<?php get_template_part("dark_footer"); ?>

<?php get_footer(); ?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script src="http://grantcardone.com/wp-content/themes/theretailer-child/js/jquery-scrolltofixed.js" type="text/javascript"></script>
<script>
	var $j = $.noConflict(true);
</script>
<script>
	$j('#floatingbanner').scrollToFixed();
</script>