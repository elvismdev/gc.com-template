<?php
/*
Template Name: 100% Width - On-Demand
*/
?>

<?php get_header(); ?>
<link href="http://a.lightspeedvt.com/v1/clients/grant_cardone/counter/styles.css" rel="stylesheet" type="text/css" />
<script src="http://a.lightspeedvt.com/v1/clients/grant_cardone/counter/init.cfm"></script>
<style type="text/css">


#floatingbanner
{display:none;}
 
.gbtr_tools_wrapper, .gbtr_header_wrapper {
	display: none;
}

.cardone_header_wrapper {
	padding-top: 39px;
	padding-bottom: 40px;
	background-color: #f4f4f4;
	margin-bottom: 30px;
	border-bottom: 5px solid #F89938;
}

.rev_slider_wrapper {
	z-index: 0;
}

.intro_video {
	z-index: 1;
	position: relative;
	margin-top: -100px;
	border: 10px solid #ffffff;
	-webkit-box-shadow:  0px 0px 10px 0px rgba(0, 0, 0, .25);
	box-shadow:  0px 0px 10px 0px rgba(0, 0, 0, .25);
}

.number h1 {
	font-family: 'Conv_HelveticaNeueLTPro-Cn';
	font-size: 40px;
	color: #115E9E;
	text-align: right;
	padding: 24px 0 0 0;
}

.inputB {
	background: #f4f4f4;
	border: none;
	padding: 7px;
	font-family: "Helvetica Neue", Helvetica, Arial;
	font-size: 12px;
	width: 94%;
}

a.image-video {
	width: 100%;
	position: relative;
	display: block;
}

a.image-video span.icon-play {
	position: absolute;
	left: 0;
	top: 0;
	filter: alpha(opacity=50);
	-moz-opacity: 0.5;
	-khtml-opacity: 0.5;
	opacity: 0.5;
}

a.image-video:hover span.icon-play {
	filter: alpha(opacity=90);
	-moz-opacity: 0.9;
	-khtml-opacity: 0.9;
	opacity: 0.9;
}

a.image-video .title-video {
	width: 100%;
	font-family: 'Conv_HelveticaNeueLTPro-BdCn';
	color: #ffffff;
	font-size: 24px;
	position: relative;
	top: -30px;
	margin: 0 auto;
	text-align: center;
	text-shadow: 0px 0px 20px #000000;
	filter: dropshadow(color=#000000, offx=0, offy=0);
	text-transform: uppercase;
	filter: alpha(opacity=95);
	-moz-opacity: 0.95;
	-khtml-opacity: 0.95;
	opacity: 0.95;
}

#free-trial {
	font-family: 'Conv_HelveticaNeueLTPro-BdCn';
	font-size: 46px;
	padding: 0;
	color: #1e64a3 !important;
}

#insightly_firstName, #insightly_lastName, #insightly_organization, #insightly_role, #insightly_background, .insightly_email, .insightly_phone  {
	background: #f4f4f4;
	border: none;
	padding: 7px;
	font-family: "Helvetica Neue", Helvetica, Arial;
	font-size: 12px;
	margin: 0 5px 5px 0;
	width: 29.5%;
}

.modal h2 {
	color: #000000;
	text-align: center;
	padding: 7px 0 15px;
	font-family: "Helvetica Neue", Helvetica, Arial;
}

.modal {
	padding: 20px;
}

.modal p {
	color: #000000;
	margin: 0 0 4px;
	font-size: 10px;
}

.modal iframe {
	z-index: 0;
}

.modal .form {
	z-index: 1;
	position: relative;
}

#sb-wrapper-inner {
	border: 0 !important;
}



@media only screen and (max-width: 479px) {
	#insightly_firstName, #insightly_lastName, #insightly_organization, #insightly_role, #insightly_background, .insightly_email, .insightly_phone {
		width: 97%;
	}
	.modal {
		margin-top: -40px !important;
	}
	.modal .form {
		margin-top: -38px !important;
	}

}

</style>

<div class="cardone_header_wrapper">
	<div class="container_12">
		<div class="grid_6 number">
			<img src="http://www.grantcardone.com/wp-content/uploads/cardone-logo-on-demand.png" width="100%"/>
		</div>
		<div class="grid_6 number">
			<h1>800-368-5771</h1>
		</div>
	</div>
</div>

	<?php while ( have_posts() ) : the_post(); ?>
        
        <div class="page_full_width">
            <div class="entry-content">
                <div class="">
                	<?php echo do_shortcode( '[rev_slider on-demand]' ) ?>
	                	<div class="shortcode_container">

                    <!-- jQuery.js -->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>

					<link rel="stylesheet" type="text/css" href="http://grantcardone.com/wp-content/themes/theretailer-child/js/shadowbox/shadowbox.css">
					<script type="text/javascript" src="http://grantcardone.com/wp-content/themes/theretailer-child/js/shadowbox/shadowbox.js"></script>
					
						<script type="text/javascript">
						Shadowbox.init({
						    handleOversize: "drag",
						    modal: true,
						    enableKeys: false
						});
						</script>

						<!-- <script type="text/javascript">
						$(function() {
							window.setTimeout(function() {
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://googleapps.insight.ly/WebToContact/Create" onsubmit="return validateForm()" method="post"><input type="hidden" name="formId" value="iAnHff0Sa+gKDtAMuAI0Ow=="/><input id="insightly_firstName" name="FirstName" placeholder="First Name*" type="text"/><input id="insightly_lastName" name="LastName" placeholder="Last Name*" type="text"/><input id="insightly_organization" name="Organization" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="insightly_role" name="Role" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="emails[0]_Value" class="insightly_email" name="emails[0].Value" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="phones[0]_Value" class="insightly_phone" name="phones[0].Value" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="insightly_background" name="background" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            }); } , 21000); }); </script> -->

						<script type="text/javascript">
						$(function() {
							$('#get-started').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#new-hire').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63107522?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/d20123bb1eabed8d1b433491576ee7de" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="d20123bb1eabed8d1b433491576ee7de" /><input name="inf_form_name" type="hidden" value="New Hire" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#sales-meeting').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63111349?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/9fda814db227dda01941cff33c5d4387" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="9fda814db227dda01941cff33c5d4387" /><input name="inf_form_name" type="hidden" value="Sales" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#road-to-sale').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63103406?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/33252dbc4ff688c23f5eaa9872ff6016" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="33252dbc4ff688c23f5eaa9872ff6016" /><input name="inf_form_name" type="hidden" value="Road to Sale" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#objections').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63110269?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/db0300dbaef7d88a3815465534304ec7" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="db0300dbaef7d88a3815465534304ec7" /><input name="inf_form_name" type="hidden" value="Objections" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#follow-up').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#phones-bdc').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><iframe src="//player.vimeo.com/video/63109963?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;wmode=transparent" width="100%" height="260" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/4760888c7e5a4edfbe839a8fa01570b4" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="4760888c7e5a4edfbe839a8fa01570b4" /><input name="inf_form_name" type="hidden" value="Phone & BDC" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 521,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#prospecting').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<script type="text/javascript">
						$(function() {
							$('#internet-leads').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

<script type="text/javascript">
						$(function() {
							$('#closing-negotiating').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

<script type="text/javascript">
						$(function() {
							$('#motivation').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

<script type="text/javascript">
						$(function() {
							$('#success-traits').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

<script type="text/javascript">
						$(function() {
							$('#management-training').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

<script type="text/javascript">
						$(function() {
							$('#role-playing').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

<script type="text/javascript">
						$(function() {
							$('#service-advisor').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

<script type="text/javascript">
						$(function() {
							$('#cardone-hiring-assistant').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

<script type="text/javascript">
						$(function() {
							$('#disc-assesment').bind('click', function(event) { 
								Shadowbox.open({
				                	content: '<div class="modal"><img src="http://grantcardone.com/wp-content/themes/theretailer-child/images/cardone-ondemand-form-top.png" width="100%" style="position: relative; z-index: 99; margin-top: -213px;" /><div class="form"><h2>Complete the form below to activate your Free Trial.</h2><form name="insightly_web_to_contact" action="https://sp204.infusionsoft.com/app/form/process/64be4b3826b39023cd334f205f683f60" onsubmit="return validateForm()" method="post"><input name="inf_form_xid" type="hidden" value="64be4b3826b39023cd334f205f683f60" /><input name="inf_form_name" type="hidden" value="Request a Free Trial" /><input name="infusionsoft_version" type="hidden" value="1.33.0.43" /><input id="inf_field_FirstName" class="insightly_phone" name="inf_field_FirstName" placeholder="First Name*" type="text"/><input id="inf_field_LastName" class="insightly_phone" name="inf_field_LastName" placeholder="Last Name*" type="text"/><input id="inf_field_Company" class="insightly_phone" name="inf_field_Company" placeholder="Organization*" type="text" style="margin-right:0;"/><input id="inf_field_JobTitle" class="insightly_phone" name="inf_field_JobTitle" placeholder="Role*" type="text"/><input type="hidden" name="emails[0].Label" value="Work"/><input id="inf_field_Email" class="insightly_email" name="inf_field_Email" placeholder="Email*" type="text"/><input type="hidden" name="phones[0].Label" value="Work"/><input id="inf_field_Phone1" class="insightly_phone" name="inf_field_Phone1" placeholder="Phone*" type="text" style="margin-right:0;"/><textarea id="inf_custom_BackgroundInformation0" class="insightly_phone" name="inf_custom_BackgroundInformation0" placeholder="Background Information" style="width: 97%; margin: 0;"></textarea><br /><p>*These fields are required and must be valid.</p><!--<img src="https://www.grantcardone.com/wp-content/uploads/cardone-footer-demand-logo.png" width="320px"/>--><input type="submit" value="Request Trial" class="button-orange" style="float: right;"/></form></div></div>',
				                    player:  "html",
				                    height: 278,				                    
				                    options: {
										modal: false
				                    }
					            });
							});
						});
						</script>

						<br class="clear" />

						<script>
						function validateForm() {
							var x=document.forms["insightly_web_to_contact"]["FirstName"].value;
							if (x==null || x=="") {
								alert("First Name must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["LastName"].value;
							if (x==null || x=="") {
								alert("Last Mame must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["Organization"].value;
							if (x==null || x=="") {
								alert("Organization must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["Role"].value;
							if (x==null || x=="") {
								alert("Role must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["emails[0].Value"].value;
							if (x==null || x=="") {
								alert("Email must be filled out.");
								return false;
							}
							var x=document.forms["insightly_web_to_contact"]["phones[0].Value"].value;
							if (x==null || x=="") {
								alert("Phone must be filled out.");
								return false;
							}
						}
						</script>					

                    <br class="clear" />


                    <div class="content_grid_3">
                    	<h2 style="padding: 10px 0;">Start Converting Now!</h2>
                    	<p style="font-size: 10px; padding: 0;">Request a</p>
                    	<h2 id="free-trial">FREE TRIAL</h2>
                    	<a  id="get-started" class="button" style="text-align: right;" href="#">Get Started Here</a>
                    </div>

                    <div class="content_grid_6">
                    	<div class="intro_video" style="max-width: 460px;">
                    		<iframe src="//player.vimeo.com/video/63090745?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    	</div>
                    </div>

                    <div class="content_grid_3">
                    	<h2 style="padding: 10px 0;">Member Login</h2>
                    	<form action="https://login.lightspeedvt.com/actions.cfm?lg=447&amp;v=35" method="POST">
							<input type="hidden" name="action" value="login">
							<input class="inputB" name="username" type="text" placeholder="Username">
							<input class="inputB" name="password" type="password" placeholder="Password">
							<input type="submit" class="button" style="width: 80px; float: right; margin: 3px 0 0 0;" value="Login">
							<a style="font-size: 10px;" href="http://login.lightspeedvt.com/forgot_password.cfm?lg=21" target="_blank">Forgot your password?</a>
						</form>
                    </div>



                    <br class="clear" />

                    <div class="content_hr" style="margin-top:30px;margin-bottom:24px"></div>

                    <div id="social">
                        <div id="social-center">
                            <div class="twitter" data-url="http://www.cardoneondemand.com" data-text="<?php the_title() ?>" data-title="Tweet" title="Twitter"></div>
                            <div class="facebook" data-url="http://www.cardoneondemand.com" data-text="<?php the_title() ?>" data-title="Like" title="Facebook"></div>
                            <div class="googleplus" data-url="http://www.cardoneondemand.com" data-text="<?php the_title() ?>" data-title="+1" title="Google Plus"></div>
                            <div class="linkedin" data-url="http://www.cardoneondemand.com" data-text="<?php the_title() ?>" data-title="Share" title="LinkedIn"></div>
                        </div>
                    </div>
                    <div class="content_hr" style="margin-top:28px;margin-bottom:10px"></div>

	                	<br class="clear" />



	                	<div class="content_grid_3" id="new-hire">
							<a class="image-video" href="#" rel="" title="New Hire">
								<img src="http://www.grantcardone.com/wp-content/uploads/01-new-hire-training.jpg" width="100%" alt="New Hire" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

	                	<div class="content_grid_3" id="sales-meeting">
							<a class="image-video" href="#" rel="" title="Sales Meeting">
								<img src="http://www.grantcardone.com/wp-content/uploads/02-sales-meetings.jpg" width="100%" alt="Sales Meeting" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

	                	<div class="content_grid_3" id="road-to-sale">
							<a class="image-video" href="#" rel="" title="Road to Sale">
								<img src="http://www.grantcardone.com/wp-content/uploads/03-road-to-sale.jpg" width="100%" alt="Road to Sale" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

	                	<div class="content_grid_3" id="objections">
							<a class="image-video" href="#" rel="" title="Objections">
								<img src="http://www.grantcardone.com/wp-content/uploads/04-handling-objection.jpg" width="100%" alt="Objections" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

						<br class="clear" />

<div class="content_grid_3" id="phones-bdc">
							<a class="image-video" href="#" rel="" title="Phones & BDC">
								<img src="http://www.grantcardone.com/wp-content/uploads/05-phones.jpg" width="100%" alt="Phones & BDC" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>
	                	
	                	<div class="content_grid_3" id="follow-up">
							<a class="image-video" href="#" rel="" title="Follow Up">
								<img src="http://www.grantcardone.com/wp-content/uploads/06-follow-up.jpg" width="100%" alt="Follow Up" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>


	                	<div class="content_grid_3" id="prospecting">
							<a class="image-video" href="#" rel="" title="Prospecting">
								<img src="http://www.grantcardone.com/wp-content/uploads/07-prospecting.jpg" width="100%" alt="Prospecting" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

	                	<div class="content_grid_3" id="internet-leads">
							<a class="image-video" href="#" rel="" title="Internet Leads">
								<img src="http://www.grantcardone.com/wp-content/uploads/08-internet-leads.jpg" width="100%" alt="Internet Leads" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

<br class="clear" />

<div class="content_grid_3" id="closing-negotiating">
							<a class="image-video" href="#" rel="" title="Closing Negotiating">
								<img src="http://www.grantcardone.com/wp-content/uploads/09-closing-negotiating.jpg" width="100%" alt="Closing Negotiating" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

<div class="content_grid_3" id="motivation">
							<a class="image-video" href="#" rel="" title="Closes">
								<img src="http://www.grantcardone.com/wp-content/uploads/10-motivation.jpg" width="100%" alt="Closes" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

<div class="content_grid_3" id="success-traits">
							<a class="image-video" href="#" rel="" title="Closes">
								<img src="http://www.grantcardone.com/wp-content/uploads/11-success-traits.jpg" width="100%" alt="Closes" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

<div class="content_grid_3" id="management-training">
							<a class="image-video" href="#" rel="" title="Closes">
								<img src="http://www.grantcardone.com/wp-content/uploads/12-management-training.jpg" width="100%" alt="Closes" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

<br class="clear" />

<div class="content_grid_3" id="role-playing">
							<a class="image-video" href="#" rel="" title="Closes">
								<img src="http://www.grantcardone.com/wp-content/uploads/13-role-playing.jpg" width="100%" alt="Closes" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

<div class="content_grid_3" id="service-advisor">
							<a class="image-video" href="#" rel="" title="Closes">
								<img src="http://www.grantcardone.com/wp-content/uploads/14-service-advisor.jpg" width="100%" alt="Closes" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

<div class="content_grid_3" id="cardone-hiring-assistant">
							<a class="image-video" href="#" rel="" title="Closes">
								<img src="http://www.grantcardone.com/wp-content/uploads/15-cardone-hiring-assistant.jpg" width="100%" alt="Closes" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

<div class="content_grid_3" id="disc-assesment">
							<a class="image-video" href="#" rel="" title="Closes">
								<img src="http://www.grantcardone.com/wp-content/uploads/16-disc-assesment.jpg" width="100%" alt="Closes" />
								<span class="icon-play"><img src="http://www.grantcardone.com/wp-content/uploads/cardone-on-demand-play.png" width="100%" alt="Play"></span>
							</a>
						</div>

	                </div>

	                	<br class="clear" />
						<br class="clear" />

	                	<?php echo do_shortcode( '[ditty_news_ticker id="11716"]' ) ?>
	                    <br class="clear" />                
	                	<br class="clear" />

                </div>
            </div><!-- .entry-content -->
        </div>

    <?php endwhile; // end of the loop. ?>

<?php get_template_part("light_footer"); ?>
<?php get_template_part("dark_footer"); ?>

<?php get_footer(); ?>