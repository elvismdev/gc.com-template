<?php
/*
Template Name: 100% Width - Single Footer Zone On
*/
?>

<?php get_header(); ?>

<style type="text/css">

#global_wrapper {
	background:  #00082c url('https://www.grantcardone.com/wp-content/uploads/cardone-page-zone-background.jpg') center 140px no-repeat;
}

.accordion {
	width: 940px;
}

.accordion-inner {
	padding: 0 !important;
}

</style>

	<?php while ( have_posts() ) : the_post(); ?>
        
        <div class="page_full_width">
            <div class="entry-content">
                <div class="">
                    <div class="shortcode_container" style="background: #ffffff;">

                        <br class="clear" />
                        
                        <div class="content_grid_12">
                            <img style="display: block; margin: 2px auto 0 auto;" src="../../wp-content/uploads/cardone-zone-logo.png" alt="cardone-zone-logo">

                            <br class="clear" />
                                <div id="social">
                                    <div id="social-center">
                                        <div class="twitter" data-url="http://www.grantcardone.com/cardonezone" data-text="<?php the_title() ?>" data-title="Tweet" title="Twitter"></div>
                                        <div class="facebook" data-url="http://www.grantcardone.com/cardonezone" data-text="<?php the_title() ?>" data-title="Like" title="Facebook"></div>
                                        <div class="googleplus" data-url="http://www.grantcardone.com/cardonezone" data-text="<?php the_title() ?>" data-title="+1" title="Google Plus"></div>
                                        <div class="linkedin" data-url="http://www.grantcardone.com/cardonezone" data-text="<?php the_title() ?>" data-title="Share" title="LinkedIn"></div>
                                    </div>

                                    <h1 style="padding: 20px 0 10px;">Call in to talk to Grant LIVE: 305-865-8668</h1>

                                </div>
                            <br class="clear" />

                            <iframe width="100%" height="59" src="//www.youtube.com/embed/b066dlsqx9Q?rel=0" frameborder="0" allowfullscreen></iframe>

                            <iframe width="100%" scrolling="no" id="ustream" frameborder="0" style="margin-top: 20px; border-bottom: 1px solid #cccccc;" src="http://www.ustream.tv/socialstream/8545209"></iframe>
                        </div>



                        <?php the_content(); ?>

                    </div>
                </div>
            </div><!-- .entry-content -->
        </div>

    <?php endwhile; // end of the loop. ?>

<?php get_template_part("dark_footer"); ?>

<?php get_footer(); ?>