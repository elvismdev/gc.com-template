<?php
/*
Template Name: 100% Width
*/
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <div class="page_full_width">
        <div class="entry-content">
            <div class="">
               <?php echo do_shortcode( '[rev_slider home]' ) ?>
               <?php //echo do_shortcode( '[ditty_news_ticker id="8873"]' ) ?>
               <!-- Output the GC Stats -->
               <?php echo do_shortcode( '[gc_stats]' ) ?>
               <div class="empty_separator" style="margin-top:10px;margin-bottom:30px"></div>
               <?php the_content(); ?>    
           </div>
       </div><!-- .entry-content -->
       <?php echo do_shortcode( '[ditty_news_ticker id="11716"]' ) ?>
       <br class="clear" /> 
   </div>

<?php endwhile; // end of the loop. ?>

<?php get_template_part("light_footer"); ?>
<?php get_template_part("dark_footer"); ?>

<?php get_footer(); ?>