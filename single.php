<?php
/**
 * The Template for displaying all single posts.
 *
 * @package theretailer
 * @since theretailer 1.0
 */

get_header(); ?>

<div class="global_content_wrapper">

<div class="container_12">

    <div class="grid_8">

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
                
                <div class="clr"></div>

				<?php theretailer_content_nav( 'nav-below' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template( '', true );
				?>

			<?php endwhile; // end of the loop. ?>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

	</div>
    
    <div class="grid_4">
    
		<div class="gbtr_aside_column">
			<?php 
			get_sidebar();
			?>
        </div>
        
    </div>

<?php echo do_shortcode( '[container]' ) ?>

<?php echo do_shortcode( '[custom_featured_products title="Featured Products"]' ) ?>


</div>

</div>

<?php get_template_part("light_footer"); ?>

<?php get_template_part("dark_footer"); ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script src="http://grantcardone.com/wp-content/themes/theretailer-child/js/jquery-scrolltofixed.js" type="text/javascript"></script>
<script>
	var $j = $.noConflict(true);
</script>
<script>
	$j('#floatingbanner').scrollToFixed();
</script>
<?php get_footer(); ?>