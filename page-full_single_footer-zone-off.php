<?php
/*
Template Name: 100% Width - Single Footer Zone Off
*/
?>

<?php get_header(); ?>

<style type="text/css">

#global_wrapper {
	background:  #00082c url('https://www.grantcardone.com/wp-content/uploads/cardone-page-zone-background.jpg') center 140px no-repeat;
}

.accordion {
	width: 940px;
}

.accordion-inner {
	padding: 0 !important;
}

</style>

	<?php while ( have_posts() ) : the_post(); ?>
        
        <div class="page_full_width">
            <div class="entry-content">
                <div class="">
                    <div class="shortcode_container" style="background: #ffffff;">

                        <div class="content_grid_2_3">
                            <img style="display: block; margin: 30px auto 0 auto;" src="../../wp-content/uploads/cardone-zone-logo.png" alt="cardone-zone-logo">

                            <br class="clear" />
                                <div id="social">
                                    <div id="social-center">
                                        <div class="twitter" data-url="http://www.grantcardone.com/cardonezone" data-text="<?php the_title() ?>" data-title="Tweet" title="Twitter"></div>
                                        <div class="facebook" data-url="http://www.grantcardone.com/cardonezone" data-text="<?php the_title() ?>" data-title="Like" title="Facebook"></div>
                                        <div class="googleplus" data-url="http://www.grantcardone.com/cardonezone" data-text="<?php the_title() ?>" data-title="+1" title="Google Plus"></div>
                                        <div class="linkedin" data-url="http://www.grantcardone.com/cardonezone" data-text="<?php the_title() ?>" data-title="Share" title="LinkedIn"></div>
                                    </div>
                                </div>
                            <br class="clear" />

                            <iframe width="560" height="315" src="//www.youtube.com/embed/videoseries?list=PLnZ0O70oWnJaFOgjWLwcrAGIHDPiMBND5" frameborder="0" allowfullscreen></iframe>

                            <h2 class="bold_title" style="margin-bottom: 45px;"><span>Next Episode In:</span></h2>
                            <iframe width="100%" scrolling="no" height="87" src="http://www.grantcardone.com/wp-content/themes/theretailer-child/flipclock/flipclock.html"></iframe>

                        </div>

                        <div class="content_grid_4">
                            <iframe width="100%" scrolling="no" id="ustream" frameborder="0" style="margin-top: 20px; border-bottom: 1px solid #cccccc;" src="http://www.ustream.tv/socialstream/8545209"></iframe>
                        </div>



                        <?php the_content(); ?>

                    </div>
                </div>
            </div><!-- .entry-content -->
        </div>

    <?php endwhile; // end of the loop. ?>

<?php get_template_part("dark_footer"); ?>

<?php get_footer(); ?>