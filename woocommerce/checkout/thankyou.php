<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( $order ) : ?>

<?php if ( $order->has_status( 'failed' ) ) : ?>

	<p><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'woocommerce' ); ?></p>

	<p><?php
		if ( is_user_logged_in() )
			_e( 'Please attempt your purchase again or go to your account page.', 'woocommerce' );
		else
			_e( 'Please attempt your purchase again.', 'woocommerce' );
		?></p>

		<p>
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
				<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>

		<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you! Your order has been received.', 'woocommerce' ), $order ); ?></p>

		<ul class="order_details">
			<li class="order">
				<?php _e( 'Order Number:', 'woocommerce' ); ?>
				<strong><?php echo $order->get_order_number(); ?></strong>
			</li>
			<li class="date">
				<?php _e( 'Date:', 'woocommerce' ); ?>
				<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
			</li>
			<li class="total">
				<?php _e( 'Total:', 'woocommerce' ); ?>
				<strong><?php echo $order->get_formatted_order_total(); ?></strong>
			</li>
			<?php if ( $order->payment_method_title ) : ?>
				<li class="method">
					<?php _e( 'Payment Method:', 'woocommerce' ); ?>
					<strong><?php echo $order->payment_method_title; ?></strong>
				</li>
			<?php endif; ?>
		</ul>
		<div class="clear"></div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
	<?php do_action( 'woocommerce_thankyou', $order->id ); ?>


	<!-- Post Affiliate Pro integration -->
	<?php if ( empty( $_GET['paypal'] ) ) : ?>


		<script id="pap_x2s6df8d" src="http://affiliate.grantcardone.com/scripts/gd48kxwjf" type="text/javascript">
		</script>
		<script type="text/javascript">
			PostAffTracker.setAccountId('default1');

			<?php
			$i = 0;
			foreach ($order->get_items() as $item) {
				$itemprice = $item['line_total'];
				$couponCode = '';
				$_product = $order->get_product_from_item($item);
				$p = $_product->id;

		try { //if coupon has been used, set the last one in the setCoupon() parameter
			$coupon = $order->get_used_coupons();
			$couponToBeUsed = (count($coupon)>1 ? count($coupon)-1 : 0);
			
			if (isset($coupon[$couponToBeUsed])) {
				$itemcount = $order->get_item_count($type = '');
				$orderdiscount = $order->get_order_discount();

				if ($itemcount > 0) {
					$discountperitem = $orderdiscount / $itemcount;
					$itemprice = $item['line_total'] - $discountperitem;
				}
				$couponCode = $coupon[$couponToBeUsed];
			}
		}
		catch (Exception $e) {
			//echo "<!--Error: ".$e->getMessage()."-->";
		}

		if (!empty($_product->sku)) {
			$p = $_product->sku;
		}
		
		echo "var sale".$i." = PostAffTracker.createSale();";
		echo "sale".$i.".setTotalCost('".$itemprice."');";
		echo "sale".$i.".setOrderID('".$order->id."_".$i."');";
		echo "sale".$i.".setProductID('".$p."');";
		echo "sale".$i.".setCurrency('".$order->get_order_currency()."');";
		echo "sale".$i.".setCoupon('".$couponCode."');";
		$i++;
	}
	?>
	PostAffTracker.register();
</script>

<?php endif; ?>
<!-- END Post Affiliate Pro integration -->


<!-- Google Code for grantcardone.com Conversion Page -->
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 973447433;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "b_j0CJ2In2EQicKW0AM";
	var google_conversion_value = 1.00;
	var google_conversion_currency = "USD";
	var google_remarketing_only = false;
	/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
	<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/973447433/?value=1.00&amp;currency_code=USD&amp;label=b_j0CJ2In2EQicKW0AM&amp;guid=ON&amp;script=0"/>
	</div>
</noscript>

<!-- Facebook Conversion Code for Checkouts - Grant Cardone (retargeting campaign) -->
<script>(function() {
	var _fbq = window._fbq || (window._fbq = []);
	if (!_fbq.loaded) {
		var fbds = document.createElement('script');
		fbds.async = true;
		fbds.src = '//connect.facebook.net/en_US/fbds.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(fbds, s);
		_fbq.loaded = true;
	}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6035956557177', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6035956557177&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>

<?php else : ?>

	<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

<?php endif; ?>
