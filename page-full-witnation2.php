<?php
/*
Template Name: 100% Width - Wit Nation Form
*/
?>

<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
        
        <div class="page_full_width">
            <div class="entry-content">
                <div class="">

                	<div class="empty_separator" style="margin-top:10px;margin-bottom:30px"></div>

                	<div class="shortcode_container">

                    <!-- jQuery.js -->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js" type="text/javascript"></script>

                    <br class="clear" />

                    <?php the_content(); ?>


                	</div>
                
                    <br class="clear" />                
                	<br class="clear" />              	

                </div>
            </div><!-- .entry-content -->
        </div>

    <?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>